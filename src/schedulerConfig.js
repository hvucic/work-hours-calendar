const evenDayWorkHours = {
  start: 480, // 08:00 in minutes
  end: 840, // 14:00 in minutes
  break: {
    start: 660, // 11:00 in minutes
    end: 690, // 11:30 in minutes
  },
};

const oddDayWorkHours = {
  start: 780, // 13:00 in minutes
  end: 1140, // 19:00 in minutes
  break: {
    start: 960, // 16:00 in minutes
    end: 990, // 16:30 in minutes
  },
};

export {
  evenDayWorkHours,
  oddDayWorkHours,
};