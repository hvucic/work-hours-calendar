import { Container } from '@material-ui/core';
import { useCallback, useState } from 'react';
import LoginDialog from './components/LoginDialog';
import Scheduler from './components/scheduler';
import './app.scss';

function App() {
  const [email, setEmail] = useState('');
  const [isDialogOpen, setIsDialogOpen] = useState(true);

  const handleDialogClose = useCallback(() => {
    setIsDialogOpen(false);
  }, []);

  const submitEmail = useCallback(email => {
    setEmail(email);
    setIsDialogOpen(false);
  }, []);

  return (
    <Container className="app">
      <header className="app-header">Clinic scheduler</header>
      <Scheduler email={email} />
      <LoginDialog
        open={isDialogOpen}
        closeDialog={handleDialogClose}
        submit={submitEmail}
      />
    </Container>
  );
}

export default App;
