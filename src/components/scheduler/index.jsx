import {
  addDays,
  eachDayOfInterval,
  endOfWeek,
  formatISO,
  isSameDay,
  isSameWeek,
  parseISO,
  startOfTomorrow,
  startOfWeek,
  subDays
} from 'date-fns';
import {
  Box,
  FormControl,
  FormControlLabel,
  InputLabel,
  MenuItem,
  Select,
  Switch
} from '@material-ui/core';
import { ScheduleOutlined } from '@material-ui/icons';
import { useCallback, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import AppointmentDialog from './AppointmentDialog';
import getRandomAppointments from '../../util/getRandomAppointments';
import DayColumn from './DayColumn';
import WeekSelect from './WeekSelect';
import './scheduler.scss';

function getAppointmentTimes(interval = 30) {
  const startTime = 8 * 60;
  const endTime = (19 * 60) + interval;
  const times = [];
  let currentTime = startTime;

  for (let i = 0; currentTime < endTime; i++) {
    const hour = Math.floor(currentTime / 60);
    const minute = currentTime % 60;
    times.push({
      value: currentTime,
      label: `${('0' + hour).slice(-2)}:${('0' + minute).slice(-2)}`,
    });
    currentTime += interval;
  }

  return times;
}

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

const WEEK_STARTS_ON_MONDAY = 1;

const Scheduler = ({ email }) => {
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [appointmentError, setAppointmentError] = useState('');
  const [takenAppointments, setTakenAppointments] = useState([]);
  const [interval, setInterval] = useState(30);
  const [weekStartsOn, setWeekStartsOn] = useState(WEEK_STARTS_ON_MONDAY);
  const history = useHistory();
  const query = useQuery();
  const queryParamDate = query.get('date');
  const appointmentTimes = getAppointmentTimes(interval);
  const selectedWeekFirstDay = startOfWeek(queryParamDate ? parseISO(queryParamDate) : new Date(), { weekStartsOn });
  const selectedWeekLastDay = endOfWeek(selectedWeekFirstDay, { weekStartsOn });
  const daysOfWeek = eachDayOfInterval({ start: selectedWeekFirstDay, end: selectedWeekLastDay });

  useEffect(() => {
    async function generateRandomAppointments() {
      const response = await getRandomAppointments(15);
      setTakenAppointments(response);
    }

    generateRandomAppointments();
  }, []);
  
  const onNextWeekClick = useCallback(() => {
    const nextWeekFirstDay = startOfWeek(addDays(selectedWeekFirstDay, 7), { weekStartsOn });
    query.set('date', formatISO(nextWeekFirstDay, { representation: 'date' }));
    history.push({ search: query.toString() });
  }, [history, selectedWeekFirstDay, query, weekStartsOn]);

  const onPrevWeekClick = useCallback(() => {
    const previousWeekFirstDay = startOfWeek(subDays(selectedWeekFirstDay, 7), { weekStartsOn });
    query.set('date', formatISO(previousWeekFirstDay, { representation: 'date' }));
    history.push({ search: query.toString() });
  }, [history, selectedWeekFirstDay, query, weekStartsOn]);

  const selectThisWeek = useCallback(() => {
    const date = weekStartsOn === WEEK_STARTS_ON_MONDAY ? new Date() : startOfTomorrow();
    query.set('date', formatISO(startOfWeek(date, { weekStartsOn }), { representation: 'date' }));
    history.push({ search: query.toString() });
  }, [history, query, weekStartsOn]);

  const handleDialogClose = useCallback(() => {
    setIsDialogOpen(false);
  }, []);
  
  const validateAppointment = useCallback((email, date) => {
    const userAppointments = takenAppointments.filter(a => (a.email === email));
    if (userAppointments.some(a => isSameDay(parseISO(a.date), date))) {
      return 'Maximum one (1) appointment can be made for a day.';
    }

    const userWeekAppointments = userAppointments.filter(a => isSameWeek(parseISO(a.date), date, { weekStartsOn }));
    if (userWeekAppointments.length > 1) {
      return 'Maximum two (2) appointments can be made for a week.';
    }
  }, [takenAppointments, weekStartsOn]);

  const submitAppointment = useCallback((date) => {
    const error = validateAppointment(email, date);

    setAppointmentError(error);

    if (error) {
      setIsDialogOpen(true);
      return;
    }
  
    setTakenAppointments([...takenAppointments, { email, date: formatISO(date) }]);
    setIsDialogOpen(true);
  }, [email, takenAppointments, validateAppointment]);

  const toggleWeekStartsOn = () => {
    let startDate;
      
    if (weekStartsOn === WEEK_STARTS_ON_MONDAY) {
      const tomorrow = startOfTomorrow();
      setWeekStartsOn(tomorrow.getDay());
      startDate = tomorrow;
    } else {
      setWeekStartsOn(WEEK_STARTS_ON_MONDAY);
      startDate = startOfWeek(new Date(), { weekStartsOn: WEEK_STARTS_ON_MONDAY });
    }

    query.set('date', formatISO(startDate, { representation: 'date' }));
    history.push({ search: query.toString() });
  };

  return (
    <Box display="flex" justifyContent="space-between">
      <Box>
        <WeekSelect
          firstDayOfWeek={selectedWeekFirstDay}
          lastDayOfWeek={selectedWeekLastDay}
          onPrevWeekClick={onPrevWeekClick}
          onNextWeekClick={onNextWeekClick}
          selectThisWeek={selectThisWeek}
        />
        <AppointmentDialog
          open={isDialogOpen}
          closeDialog={handleDialogClose}
          error={appointmentError}
        />
        <Box display="flex">
          <Box className="column">
            <div className="heading">
              <ScheduleOutlined />
            </div>
            {appointmentTimes.map(({ label }, index) => <div key={index} className="cell">{label}</div>)}
          </Box>
          <Box display="flex">
            {daysOfWeek.map((value, index) =>
              <DayColumn
                key={index}
                date={value}
                email={email}
                appointments={appointmentTimes}
                takenAppointments={takenAppointments}
                selectDate={submitAppointment}
              />
            )}
          </Box>
        </Box>
      </Box>
      <Box className="scheduler-sidebar" p={2}>
        <FormControl variant="filled" fullWidth>
          <InputLabel>Select interval (minutes)</InputLabel>
          <Select
            value={interval}
            onChange={(e) => setInterval(e.target.value)}
            fullWidth
          >
            <MenuItem value={15}>15</MenuItem>
            <MenuItem value={30}>30</MenuItem>
            <MenuItem value={60}>60</MenuItem>
          </Select>
        </FormControl>
        <FormControlLabel
          control={
            <Switch
              checked={weekStartsOn === startOfTomorrow().getDay()}
              onChange={toggleWeekStartsOn}
              color="primary"
            />
          }
          label="Start week with tomorrow"
        />
      </Box>
    </Box>
  );
}

export default Scheduler;