import { format, isThisWeek } from 'date-fns';
import { Box, Button } from '@material-ui/core';
import { ChevronLeft, ChevronRight } from '@material-ui/icons';

const WeekSelect = ({ firstDayOfWeek, lastDayOfWeek, onNextWeekClick, onPrevWeekClick, selectThisWeek }) => {
  return (
    <Box display="flex" justifyContent="space-between" my={1}>
      <Box display="flex" justifyContent="left">
        <Button onClick={onPrevWeekClick} disabled={isThisWeek(firstDayOfWeek)}>
          <ChevronLeft />
        </Button>
        <Button onClick={onNextWeekClick}>
          <ChevronRight />
        </Button>
        <Box display="flex" alignItems="center" px={2}>
          {isThisWeek(firstDayOfWeek) &&
            <Box fontWeight={700} fontSize="h5.fontSize" mr={1}>This week:</Box>
          }
          <Box fontSize="h5.fontSize">
            {format(firstDayOfWeek, 'dd')} - {format(lastDayOfWeek, 'dd MMM yyyy')}
          </Box>
        </Box>
      </Box>
      {!isThisWeek(firstDayOfWeek) &&
          <Button onClick={selectThisWeek}>Return to this week</Button>
        }
    </Box>
  );
}

export default WeekSelect;