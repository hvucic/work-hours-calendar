import { format, getDay } from 'date-fns';
import { Box, Typography } from '@material-ui/core';
import DayCell from './DayCell';

const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
const shortDateFormat = 'dd MMM';

const DayColumn = ({ appointments, date, email, selectDate, takenAppointments }) => {
  return (
    <Box display="flex" flexDirection="column" className="column">
      <Box display="flex" flexDirection="column" className="heading">
        <Typography variant="subtitle2">{daysOfWeek[getDay(date)]}</Typography>
        <Typography variant="subtitle1">{format(date, shortDateFormat)}</Typography>
      </Box>
      {appointments.map((appointment, index) =>
        <DayCell
          key={index}
          date={date}
          email={email}
          minuteOfDay={appointment.value}
          selectDate={selectDate}
          takenAppointments={takenAppointments}
        />
      )}
    </Box>
  );
}

export default DayColumn;