import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
} from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';

const AppointmentDialog = ({ closeDialog, error, open }) => {
  const severity = !!error ? 'warning' : 'success';
  const messages = {
    warning: error,
    success: 'Appointment saved successfully',
  }
  return (
    <Dialog open={open} maxWidth="xs" fullWidth>
      <DialogContent>
        <Alert severity={severity}>
          <AlertTitle className="alert-title">{severity}</AlertTitle>
          {messages[severity]}
        </Alert>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => closeDialog()} color="secondary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default AppointmentDialog;