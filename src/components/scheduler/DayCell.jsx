import { formatISO, isBefore, setHours, setMinutes } from 'date-fns';
import { amber, lightGreen, red } from '@material-ui/core/colors';
import { Button, Box, Chip, Typography } from '@material-ui/core';
import { CheckCircle } from '@material-ui/icons';
import { oddDayWorkHours, evenDayWorkHours } from '../../schedulerConfig';

const styles = {
  breakChip: { backgroundColor: amber[600], color: '#fff' },
  takenChip: { backgroundColor: red[300], color: '#fff' },
  check: { fontSize: 40, color: lightGreen[500] },
};

function isWorkingTime(date, minuteOfDay) {
  const isEvenDay = (date.getDate() % 2) === 0;
  const dayIndex = date.getDay()
  if (isBefore(date, new Date())) return false; // Today and days before today cannot be picked
  if (dayIndex < 1) return false; // Sunday is non-working
  if ((dayIndex === 6) && !isEvenDay) return false // Odd Saturdays are non-working

  return isEvenDay ? 
    (minuteOfDay >= evenDayWorkHours.start && minuteOfDay < evenDayWorkHours.end) : 
    (minuteOfDay >= oddDayWorkHours.start && minuteOfDay < oddDayWorkHours.end);
};

const DayCell = ({ date, email, minuteOfDay, selectDate, takenAppointments }) => {
  const dateTime = setHours(setMinutes(date, minuteOfDay % 60), Math.floor(minuteOfDay / 60));
  const isAvailable = !takenAppointments.find(appointment => appointment.date === formatISO(dateTime));
  const isUserAppointment = takenAppointments
    .filter(a => (a.email === email))
    .find(a => a.date === formatISO(dateTime));

  const workTimeCellContent = () => {
    if (evenDayWorkHours.break.start === minuteOfDay || oddDayWorkHours.break.start === minuteOfDay) {
      return (<Chip label="Break" style={styles.breakChip} />);
    }

    if (isAvailable) {
      return (
        <Button color="primary" size="small" variant="contained" onClick={() => selectDate(dateTime)}>
          Available
        </Button>
      );
    }

    if (isUserAppointment) {
      return (<CheckCircle style={styles.check} />)
    }

    return (<Chip style={styles.takenChip} label="Taken" />);
  };

  const nonWorkingCellContent = () => {
    if (isBefore(date, new Date())) return (<Typography variant="caption">Unavailable</Typography>);
    return (<Chip label="Not working" />)
  };

  return (
    <Box className="cell">
      {isWorkingTime(date, minuteOfDay)
        ? workTimeCellContent()
        : nonWorkingCellContent()
      }
    </Box>
  );
}

export default DayCell;