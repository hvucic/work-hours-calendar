import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core';
import { useState } from 'react';

const AppointmentDialog = ({ open, closeDialog, submit }) => {
  const [email, setEmail] = useState('');
  const [error, setError] = useState('');

  const validate = (email) => {
    if (!email) return 'This field is required.';
    if (!/^[^@\s]+@[^@\s]+\.[^@\s]+$/.test(email)) return 'Email is not valid.';
  };

  const handleConfirm = () => {
    const error = validate(email);

    if (error) {
      setError(error);
      return;
    }

    submit({ email });
    setEmail('');
    closeDialog();
  };

  const handleInputValue = (event) => {
    setEmail(event.target.value);
    const error = validate(event.target.value);

    if (error) {
      setError(error);
      return;
    };

    setError('');
  };

  return (
    <Dialog open={open} maxWidth="xs" fullWidth>
      <DialogTitle>Make an appointment</DialogTitle>
      <DialogContent>
        <DialogContentText>
          To make an appointment, please enter your email.
        </DialogContentText>
        <TextField
          autoFocus
          margin="dense"
          id="email"
          label="Your email"
          value={email}
          error={!!error}
          onChange={handleInputValue}
          onBlur={handleInputValue}
          helperText={error}
          fullWidth
          required
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleConfirm} color="primary">
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default AppointmentDialog;