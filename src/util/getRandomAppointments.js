import {
  addDays,
  formatISO,
  isSunday,
  setHours,
  setMinutes,
  setSeconds,
} from 'date-fns';
import axios from 'axios';
import { evenDayWorkHours, oddDayWorkHours } from '../schedulerConfig';

function getRandomUserEmails(count) {
  return axios.get(`https://randomuser.me/api/?results=${count}&nat=us`)
    .then(({ data }) => data.results.map(user => user.email));
}

function getRandomDate(start, end) {
  const date = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  if (isSunday(date)) {
    return getRandomDate(start, end);
  }

  return date;
}

function getRandomHour(isEvenDay) {
  const startHour = isEvenDay 
    ? Math.floor(evenDayWorkHours.start / 60)
    : Math.floor(oddDayWorkHours.start / 60);
  const endHour = isEvenDay 
    ? Math.floor((evenDayWorkHours.end - 60) / 60)
    : Math.floor((oddDayWorkHours.end - 60) / 60);
  const hour = startHour + Math.random() * (endHour - startHour) | 0;
  const evenDayBreakTime = Math.floor(evenDayWorkHours.break.start / 60);
  const oddDayBreakTime = Math.floor(oddDayWorkHours.break.start / 60);

  if ((isEvenDay && hour === evenDayBreakTime) || (!isEvenDay && hour === oddDayBreakTime)) {
    return getRandomHour(isEvenDay);
  }

  return hour;
}

function getRandomDateTime() {
  const date = getRandomDate(addDays(new Date(), 1), addDays(new Date(), 6));
  const isEvenDay = (date.getDate() % 2) === 0;
  const hour = getRandomHour(isEvenDay);

  return formatISO(setHours(setMinutes(setSeconds(date, 0), 0), hour));
}

export default async function getRandomAppointments(count = 15) {
  const appointments = [];
  const randomUserEmails = await getRandomUserEmails(count);

  while(appointments.length < count){
    const date = getRandomDateTime();
    if(!appointments.find(a => a.date === date)) {
      appointments.push({ email: randomUserEmails[appointments.length], date })
    };
  }
  
  return appointments;
}